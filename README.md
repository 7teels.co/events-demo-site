# Events Demo Site



## Used Tech Stack
- [Roots tools](https://roots.io/) - Bedrock architecture and Sage starter theme (WP template hierarchy, SCSS style preprocessor, Webpack styles and scripts bundler)
- Front-end - VueJS, jQuery, Bootstrap 4
- Back-end - WP REST API, MVC architecture (Blade template engine, Controllers, WP DB architecture for Models)
- Microdata is used for rich snippets - https://schema.org/Event
- AJAX and WP REST API (for loading More Events)



## Used plugins

__[mu-plugins](https://gitlab.com/7teels.co/events-demo-site/-/tree/master/web/app/mu-plugins) (custom)__
- [Custom Post Types Setup](https://gitlab.com/7teels.co/events-demo-site/-/tree/master/web/app/mu-plugins/cpt-setup.php) - slug for Archive page is modified to be '/events'
- [Advanced Custom Fields Setup](https://gitlab.com/7teels.co/events-demo-site/-/tree/master/web/app/mu-plugins/custom-fields-setup.php) (for performance and delivery purposes)
- [Meta Box Example](https://gitlab.com/7teels.co/events-demo-site/-/tree/master/web/app/mu-plugins/meta-box-example.php) (for demonstrating knowledge)

__plugins (3-rd party)__
- ACF Pro
- Font Awesome



## Custom code written in: [web/app/themes/events-demo-site](https://gitlab.com/7teels.co/events-demo-site/-/tree/master/web/app/themes/events-demo-site)

__[/app/filters.php](https://gitlab.com/7teels.co/events-demo-site/-/blob/master/web/app/themes/events-demo-site/app/filters.php)__
- pre_get_post() filter is used for filtering events by date/meta_key for Archive page (row 95)

### Theme setup
__[/app/setup.php](https://gitlab.com/7teels.co/events-demo-site/-/blob/master/web/app/themes/events-demo-site/app/setup.php)__
- wp_enqueue_script() call addevent script/plugin (row 16)
- acf/init set google_api_key


### Templates overview

#### Single Event
- [views/partials/content-single-event.blade.php](https://gitlab.com/7teels.co/events-demo-site/-/blob/master/web/app/themes/events-demo-site/resources/views/partials/content-single-event.blade.php) && [views/partials/event](https://gitlab.com/7teels.co/events-demo-site/-/blob/master/web/app/themes/events-demo-site/resources/views/partials/event)
- Sections
    - Header
    - Content/Event description
    - Image/Hero
    - Date/time
    - Location
    - Footer
        - add-to-calendar
        - external-url
        - offers
        - share the event (fb, Twitter, in, email)
- Comments
- More Events - loaded by Vue/Vuex

#### Archive Page
- [archive-event.blade.php](https://gitlab.com/7teels.co/events-demo-site/-/blob/master/web/app/themes/events-demo-site/resources/views/archive-event.blade.php) not modified
  - [content-event.blade.php](https://gitlab.com/7teels.co/events-demo-site/-/blob/master/web/app/themes/events-demo-site/resources/views/partials/content-event.blade.php)  (modified - event-date, event-url, event-location blocks are reused from single event template)

---
#### I look forward to being interviewed at your earliest convenience. Thank you so much for this opportunity. If you require any additional information, I can be contacted at the phone number or email listed here.
- __tel__: 0878 871 458
- __email__: sholeka@gmail.com
