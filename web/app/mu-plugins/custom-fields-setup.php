<?php
/**
 * Plugin Name: Advanced Custom Fields Setup Plugin
 * Description: A plugin that set needed ACFields for performance boost
 * Version:     1.0
 * Author:      Ivaylo Sholekov
 * Author URI:  http://sholekov.com
 * License:     GPL2
 */

add_action('acf/init', function () {

    if( function_exists('acf_add_local_field_group') ):

        acf_add_local_field_group(array(
            'key' => 'group_600eb8947ec71',
            'title' => 'Main Fields',
            'fields' => array(
                array(
                    'key' => 'field_600eb8d634f2c',
                    'label' => 'Date',
                    'name' => 'date',
                    'type' => 'date_time_picker',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'display_format' => 'Y-m-d H:i:s',
                    'return_format' => 'Y-m-d H:i:s',
                    'first_day' => 1,
                ),
                array(
                    'key' => 'field_600eb8e834f2d',
                    'label' => 'Location',
                    'name' => 'location',
                    'type' => 'google_map',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'center_lat' => '',
                    'center_lng' => '',
                    'zoom' => '',
                    'height' => '',
                ),
                array(
                    'key' => 'field_600effca635cc',
                    'label' => 'URL',
                    'name' => 'url',
                    'type' => 'url',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'event',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
        ));
        
    endif;
    
});
