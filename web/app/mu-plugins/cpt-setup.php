<?php
/*
Plugin Name: Custom Post Types Setup Plugin
Description: Setup all Custom Post Types
Plugin URI:  https://sholekov.com/plugins/cpt-setup-boilerplate/
Author:      Ivaylo Sholekov
Author URI:  http://sholekov.com
Version:     1.0
License:     GPLv2 or later
*/


/**
 * Register a custom post type called "event".
 */
function setup_post_type_event() {
    $labels = array(
        'name'                  => _x( 'Events', 'Post type general name', 'shlk' ),
        'singular_name'         => _x( 'Event', 'Post type singular name', 'shlk' ),
        'menu_name'             => _x( 'Events', 'Admin Menu text', 'shlk' ),
        'name_admin_bar'        => _x( 'Event', 'Add New on Toolbar', 'shlk' ),
        'add_new'               => __( 'Add New', 'shlk' ),
        'add_new_item'          => __( 'Add New Event', 'shlk' ),
        'new_item'              => __( 'New Event', 'shlk' ),
        'edit_item'             => __( 'Edit Event', 'shlk' ),
        'view_item'             => __( 'View Event', 'shlk' ),
        'all_items'             => __( 'All Events', 'shlk' ),
        'search_items'          => __( 'Search Events', 'shlk' ),
        'not_found'             => __( 'No Events found.', 'shlk' ),
        'not_found_in_trash'    => __( 'No Events found in Trash.', 'shlk' ),
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'event' ),
		'capability_type'    => 'post',
        'has_archive'        => 'events',
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
        'show_in_rest'       => true,
        'menu_icon'          => 'dashicons-calendar-alt',
    );
    register_post_type( 'event', $args );
}

add_action( 'init', 'setup_post_type_event' );
