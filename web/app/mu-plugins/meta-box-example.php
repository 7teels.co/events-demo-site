<?php
/*
Plugin Name: Meta Box Example Plugin
Description: Example demonstrating how to add Meta Boxes
Version:     1.0
Author:      Ivaylo Sholekov
Author URI:  http://sholekov.com/
License:     GPLv2 or later
*/



// register meta box
function myplugin_add_meta_box() {

	$post_types = array( 'event' );

	foreach ( $post_types as $post_type ) {

		add_meta_box(
			'myplugin_meta_box',
			'Meta Box Demo',
			'myplugin_display_meta_box',
			$post_type
		);

	}

}
add_action( 'add_meta_boxes', 'myplugin_add_meta_box' );



// display meta box
function myplugin_display_meta_box( $post ) {

	$value = get_post_meta( $post->ID, '_myplugin_meta_key', true );

	wp_nonce_field( basename( __FILE__ ), 'myplugin_meta_box_nonce' );

	?>

	<label for="myplugin-meta-box">Event Date:</label>
	<input type="text" id="myplugin-meta-box" name="myplugin-meta-box" value="<?= $value; ?>">

<?php

}



// save meta box
function myplugin_save_meta_box( $post_id ) {

	$is_autosave = wp_is_post_autosave( $post_id );
	$is_revision = wp_is_post_revision( $post_id );

	$is_valid_nonce = false;

	if ( isset( $_POST[ 'myplugin_meta_box_nonce' ] ) ) {

		if ( wp_verify_nonce( $_POST[ 'myplugin_meta_box_nonce' ], basename( __FILE__ ) ) ) {

			$is_valid_nonce = true;

		}

	}

	if ( $is_autosave || $is_revision || !$is_valid_nonce ) return;

	if ( array_key_exists( 'myplugin-meta-box', $_POST ) ) {

		update_post_meta(
			$post_id,
			'_myplugin_meta_key',
			sanitize_text_field( $_POST[ 'myplugin-meta-box' ] )
		);

	}

}
add_action( 'save_post', 'myplugin_save_meta_box' );


