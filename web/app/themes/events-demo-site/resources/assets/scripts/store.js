import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        events: {},
        current_event: '',
    },
    mutations: {
        setDataCurrentEvent(state) {
            state.current_event = {
                id: jQuery('body').data('post_id'),
            };
        },
        setDataRelatedEvents(state) {
            const WP_REST_URL = document.querySelector('link[rel="https://api.w.org/"]').href;
            jQuery.ajax({
                url: WP_REST_URL+'wp/v2/'+'event',
                beforeSend: () => {
                    jQuery('#loading').removeClass('d-none');
                },
                complete: () => {
                    jQuery('#loading').addClass('d-none');
                },
                success: response => {
                    console.log( response )
                    state.events = response
                    state.events.forEach(function() {
                        // 
                    })
                },
            })
        },
    },
    actions: {
        getEvents({commit}) {
            commit('setDataCurrentEvent')
            commit('setDataRelatedEvents')
        },
    },
})