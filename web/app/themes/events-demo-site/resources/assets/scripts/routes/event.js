import Vue from 'vue';
import store from '../store';

// import Vue components
import more_events from '../vue/more_events.vue';

export default {
  init() {
    // JavaScript to be fired on the events page

    window.app = window.app || {};

    window.app = new Vue({
      store,
      created() {
        this.$store.dispatch('getEvents')
      },
      render: h => h(more_events),
    }).$mount('#more-events');

    console.log('load event')
  },
};
