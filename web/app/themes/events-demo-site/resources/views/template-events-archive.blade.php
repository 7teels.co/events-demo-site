{{--
  Template Name: Events Archive Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-page')


    @if($events->have_posts())
      @while ($events->have_posts()) @php $events->the_post() @endphp

        @include('partials.content-'.get_post_type())

      @endwhile
    @endif
  @endwhile
@endsection
