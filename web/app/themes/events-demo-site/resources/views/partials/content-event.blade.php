<article @php post_class() @endphp>
  <header>
    <h1 class="entry-title">{!! get_the_title() !!}</h1>
  </header>
  <div class="entry-content">
    @php the_content() @endphp
  </div>
  <footer>

  <div class="event-date">
      @include('partials/event/date-time', [
        'date' => get_field('date'),
        'event_date' => explode(' ', get_field('date'))[0],
        'event_time' => explode(' ', get_field('date'))[1]
      ])
  </div>

  <div class="event-url">
      @include('partials/event/external-url', ['url' => get_field('url')])
  </div>

  <div class="event-location">
      @include('partials/event/venue', ['location' => get_field('location')])
  </div>

  </footer>
</article>
