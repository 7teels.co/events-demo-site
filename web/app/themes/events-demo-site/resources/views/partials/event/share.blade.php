
<div class="event-share">
    <span>Share:</span>

    @foreach($share_links as $icon => $link)
        <a target="_blank" href="{{ $link.get_permalink() }}"><i class="fa fa-{{$icon}}"></i></a>
    @endforeach

</div>
