<div class="event-venue" itemprop="location" itemscope itemtype="https://schema.org/Place">
    <div class="address" itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
        <span itemprop="streetAddress">{{ $location['street_name'] }}</span><br>
        <span itemprop="addressLocality">{{ $location['city'] }}</span>, <span itemprop="addressRegion">{{ $location['state'] }}</span> <span itemprop="postalCode">{{ $location['post_code'] }}</span>
    </div>
</div>
