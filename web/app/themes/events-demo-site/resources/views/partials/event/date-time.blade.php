
<div class="event-date" itemprop="startDate" content="{{$date}}">
    {{ $event_date }}
</div>

<meta itemprop="endDate" content="{{$date}}">

<div class="event-time">{{ $event_time }}</div>
