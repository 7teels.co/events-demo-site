
<div class="event-add-to-calendar">
      <!-- Button code -->
      <div title="Add to Calendar" class="addeventatc">
          <!-- Add to Calendar -->
          Добави към календара
          <span class="start">{{ strtoupper($date) }}</span>
          <span class="timezone">Europe/Sofia</span>
          <span class="title">{!! get_the_title() !!}</span>
      </div>
</div>
