
<div itemprop="offers" itemscope itemtype="https://schema.org/Offer">
    
    <meta itemprop="validFrom" content="2013-09-14T21:30">
    <meta itemprop="availability" content="https://schema.org/SoldOut">
    <meta itemprop="priceCurrency" content="USD">

    <div class="event-price" itemprop="price" content="13.00">$13.00</div>
    
    <a itemprop="url" href="http://www.ticketfly.com/purchase/309433">Tickets</a>

</div>