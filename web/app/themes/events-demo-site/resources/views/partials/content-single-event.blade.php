<article @php post_class('event-wrapper') @endphp itemscope itemtype="https://schema.org/Event">
  <meta itemprop="eventStatus" content="https://schema.org/EventScheduled">

  <!-- <div class="additional-stuff">
    <a href="https://search.google.com/test/rich-results?url={{ get_permalink() }}&user_agent=1" target="_blank">test schema markup</a>
  </div> -->

  <header>
    <h1 class="event-title" itemprop="name">{!! get_the_title() !!}</h1>
  </header>

  <div class="event-content" itemprop="description">
    @php the_content() @endphp
  </div>

  @include('partials/event/feature-image')
  @include('partials/event/date-time')
  @include('partials/event/venue')

  <footer>
    @include('partials/event/add-to-calendar')
    @include('partials/event/external-url')
    @include('partials/event/offers')
    @include('partials/event/share')
  </footer>

  @php comments_template('/partials/comments.blade.php') @endphp

  <div id="more-events"></div>

</article>
