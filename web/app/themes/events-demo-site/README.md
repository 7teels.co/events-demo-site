## Custom code written in:

__[/app/filters.php](https://gitlab.com/7teels.co/events-demo-site/-/blob/master/web/app/themes/events-demo-site/app/filters.php)__
- pre_get_post() filter is used for filtering events by date/meta_key for Archive page (row 95)

### Theme setup
__[/app/setup.php](https://gitlab.com/7teels.co/events-demo-site/-/blob/master/web/app/themes/events-demo-site/app/setup.php)__
- wp_enqueue_script() call addevent script/plugin (row 16)
- acf/init set google_api_key


### Templates overview

#### Single Event
- [views/partials/content-single-event.blade.php](https://gitlab.com/7teels.co/events-demo-site/-/blob/master/web/app/themes/events-demo-site/resources/views/partials/content-single-event.blade.php) && [views/partials/event](https://gitlab.com/7teels.co/events-demo-site/-/blob/master/web/app/themes/events-demo-site/resources/views/partials/event)
- Sections
    - Header
    - Content/Event description
    - Image/Hero
    - Date/time
    - Location
    - Footer
        - add-to-calendar
        - external-url
        - offers
        - share the event (fb, Twitter, in, email)
- Comments
- More Events - loaded by Vue/Vuex

#### Archive Page
- [archive-event.blade.php](https://gitlab.com/7teels.co/events-demo-site/-/blob/master/web/app/themes/events-demo-site/resources/views/archive-event.blade.php) not modified
  - [content-event.blade.php](https://gitlab.com/7teels.co/events-demo-site/-/blob/master/web/app/themes/events-demo-site/resources/views/partials/content-event.blade.php)  (modified - event-date, event-url, event-location blocks are reused from single event template)
