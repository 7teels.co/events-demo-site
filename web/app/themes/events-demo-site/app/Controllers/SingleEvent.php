<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleEvent extends Controller
{
    protected $acf = true;
    protected $date_time;

    // needs to be refactored (obviously)
    protected $share_links = [
        'facebook' => 'https://www.facebook.com/sharer/sharer.php?u=',
        'twitter' => 'https://twitter.com/intent/tweet?',
        'linkedin' => 'https://www.linkedin.com/shareArticle?mini=true&url=',
        'envelope' => 'mailto:info@example.com?&subject=&body=',
    ];

    function __constructor()
    {
        $this->date_time = explode(' ', get_field('date'));
    }

    public function thumbnail()
    {
        $thumbnail = '<figure itemprop="image" itemscope itemtype="http://schema.org/ImageObject">';
        $thumbnail .= get_the_post_thumbnail( null, 'full', ['class' => 'w-100 h-auto', 'itemprop' => 'url'] );
        $thumbnail .= '</figure>';
        return $thumbnail;
    }

    public function event_date()
    {
        return $this->date_time[0];
    }

    public function event_time()
    {
        return $this->date_time[1];
    }

    public function share_links()
    {
        return $this->share_links;
    }

    public function location()
    {
        return get_field('location'); // return array instead object
    }
}
