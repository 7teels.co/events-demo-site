<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateEventsArchive extends Controller
{

    public function events()
    {
        $the_query = new \WP_Query([
            'post_type'		    => 'event',
            'posts_per_page'	=> -1,
            'orderby'           => 'ASC',
            'post_parent'       => 0,
        ]);

        return $the_query;
    }


}
